var components = {
    GoogleMapsProvider: function () { return import(
            /* webpackChunkName: "google-maps-provider" */ './GoogleMapsProvider-b0e0e51a.js'
        ); },
};

function index (Vue) {
    for (var name in components) {
        Vue.component(name, components[name]);
    }
}

export default index;
