var CALLBACK_NAME = 'gmapsCallback';

var initialized = false;
var resolveInitPromise;
var rejectInitPromise;
// This promise handles the initialization
// status of the google maps script.
var initPromise = new Promise(function (resolve, reject) {
    resolveInitPromise = resolve;
    rejectInitPromise = reject;
});

function init (apiKey) {
    // If Google Maps already is initialized
    // the `initPromise` should get resolved
    // eventually.
    if (initialized) { return initPromise; }

    initialized = true;
    // The callback function is called by
    // the Google Maps script if it is
    // successfully loaded.
    window[CALLBACK_NAME] = function () { return resolveInitPromise(window.google); };

    // We inject a new script tag into
    // the `<head>` of our HTML to load
    // the Google Maps script.
    var script = document.createElement('script');
    script.async = true;
    script.defer = true;
    script.src = "https://maps.googleapis.com/maps/api/js?key=" + apiKey + "&callback=" + CALLBACK_NAME;
    script.onerror = rejectInitPromise;
    document.querySelector('head').appendChild(script);

    return initPromise;
}

var debounce = function (callback, interval) {
    var debounceTimeoutId;

    return function () {
        var this$1 = this;
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        clearTimeout(debounceTimeoutId);
        debounceTimeoutId = setTimeout(
            function () { return callback.apply(this$1, args); },
            interval
        );
    };
};

var script = {
    props: {
        coordinates: {
            type: Array,
            required: true
        },

        markerIcon: {
            type: String,
            required: true
        },

        markerWidth: {
            type: Number,
            required: true
        },

        markerHeight: {
            type: Number,
            required: true
        },

        apiKey: {
            type: String,
            required: true
        },

        zoom: {
            type: Number,
            required: true
        },

        mapStyle: {
            type: Array,
            default: null
        }
    },

    data: function () { return ({
        markers: [],
        google: null,
        map: null,
        listenerHandler: null
    }); },

    mounted: async function mounted () {
        this.google = await init(this.apiKey);

        this.init();

        this.listenerHandler = this.google.maps.event
            .addDomListener(window, 'resize', debounce(this.handleRezise, 250));
    },

    beforeDestroy: function beforeDestroy () {
        this.google.maps.event.removeListener(this.listenerHandler);
    },

    methods: {
        init: function init () {
            this.map = this.createMap();

            this.markers = this.createMarkers();

            if (this.coordinates.length > 1) {
                this.fitAllMarkers();
            }
        },

        createMap: function createMap () {
            return new this.google.maps.Map(this.$el, {
                mapTypeControl: false,
                streetViewControl: false,
                fullscreenControl: false,
                zoomControl: false,
                center: this.getPosition(this.coordinates[0]),
                zoom: this.zoom,
                styles: this.mapStyle
            });
        },

        createMarkers: function createMarkers () {
            var this$1 = this;

            var icon = this.getIcon();

            return this.coordinates.map(function (coordinate) { return new this$1.google.maps.Marker({
                position: this$1.getPosition(coordinate),
                animation: this$1.google.maps.Animation.DROP,
                map: this$1.map,
                icon: icon
            }); });
        },

        getPosition: function getPosition (ref) {
            var lat = ref.lat;
            var lng = ref.lng;

            return {
                lat: Number(lat),
                lng: Number(lng)
            };
        },

        getIcon: function getIcon () {
            var iconSize = this.getIconSize();

            return {
                url: this.markerIcon,
                size: iconSize,
                scaledSize: iconSize
            };
        },

        getIconSize: function getIconSize () {
            var screenWidth = window.innerWidth;
            var multiplier = 1;

            if (screenWidth < 600) {
                multiplier = 0.8;
            } else if (screenWidth < 1030) {
                multiplier = 0.9;
            }

            return new this.google.maps.Size(this.markerWidth * multiplier, this.markerHeight * multiplier);
        },

        fitAllMarkers: function fitAllMarkers () {
            var bounds = new this.google.maps.LatLngBounds();

            this.markers.forEach(function (marker) {
                bounds.extend(marker.position);
            });

            this.map.fitBounds(bounds);
        },

        handleRezise: function handleRezise () {
            this.resizeMarkers();

            if (this.coordinates.length > 1) {
                this.fitAllMarkers();
            } else {
                this.centerMap();
            }
        },

        centerMap: function centerMap () {
            var center = this.getPosition(this.coordinates[0]);

            this.map.panTo(center);
        },

        resizeMarkers: function resizeMarkers () {
            var icon = this.getIcon();

            this.markers.forEach(function (marker) {
                marker.setIcon(icon);
            });
        }
    },

    render: function render () {
        return this.$scopedSlots.default();
    }
};

export default script;
