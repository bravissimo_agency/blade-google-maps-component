import buble from '@rollup/plugin-buble';
import vue from 'rollup-plugin-vue';

export default {
  input: 'src/js/index.js',
  output: {
    format: 'es',
    dir: 'dist',
  },
  plugins: [
    vue(),
    buble({
      transforms: { asyncAwait: false }
    })
  ]
}