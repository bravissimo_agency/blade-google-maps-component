<?php

namespace Bravissimo\BladeGoogleMapsComponent;

use Illuminate\Support\ServiceProvider;

class BladeGoogleMapsComponentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__, 'googleMaps');
    }
}
